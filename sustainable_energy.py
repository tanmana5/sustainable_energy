# -*- coding: utf-8 -*-
'''
ARIMA model building steps @
http://machinelearningmastery.com/arima-for-time-series-forecasting-with-python/

This code explores data downloaded from the UN data portal to get an understanding of the type 
of data available, trends observed and some basic analysis to see if possible to use for 
performing predictive analysis for environmental data.
'''
import os
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot
from pandas.tools.plotting import autocorrelation_plot
from statsmodels.tsa.arima_model import ARIMA
from pandas import DataFrame
from sklearn.metrics import mean_squared_error


# local filepath
path = '/Users/s1936157/Downloads/Data_Extract_From_Sustainable_Energy_for_All'
filename = 'sustainable_dev.txt'


# exploring data
names = ['Country Name', 'Country Code', 'Time',
         'FINAL_ENERGY_INTENSITY', 'PRIMARY_ENERGY_INTENSITY',
         'HOUSEHOLD_ENERGY_INTENSITY', 'AGR_ENERGY_INTENSITY',
         'INDUSTRY_ENERGY_INTENSITY', 'HOUSEHOLD_ENERGY_INTENSITY',
         'SERVICES_ENERGY_INTENSITY', 'TRANS_ENERGY_INTENSITY',
         'ENERGY_SAVINGS', 'ENERGY_SAVINGS', 'FINAL_PRIMARY_RATIO',
         'GDP_PPP']
df = pd.read_csv(os.path.join(path, filename), sep='\t',
                 encoding='utf-16', usecols=names)
# df.describe()
top_n = df.sort('FINAL_ENERGY_INTENSITY', ascending=False)\
  .head(10)[['Country Name', 'Country Code', 'Time',
             'PRIMARY_ENERGY_INTENSITY']]
df = df.replace('..', np.nan)
df = df.dropna(axis=0, how='any')
year_list = [str(i) for i in range(1990, 2013)]
df = df[df['Time'].isin(year_list)]
country_list = ['High income', 'Low income', 'Lower middle income',
                'Upper middle income']
df = df[df['Country Name'].isin(country_list)]
df = df.convert_objects(convert_dates=True, convert_numeric=True, copy=True)
df['Country Name'] = [x for x in df['Country Name'] if str(x) != '']
df['Time'] = [x for x in df['Time'] if str(x) != '']
df_pivot = df.pivot('Country Name', 'Time', 'FINAL_ENERGY_INTENSITY')
df_pivot = df_pivot.dropna(axis=0, how='any')

# plotting a heatmap
fig, ax = plt.subplots(figsize=(8, 8))
ax = sns.heatmap(df_pivot, annot=True, fmt='.2f')

# Time series plot
ax.set_ylabel('FINAL_ENERGY_INTENSITY')
df_pivot.transpose().plot(ax=ax)
df_pivot = df_pivot.transpose()

# pair plot
names = ['FINAL_ENERGY_INTENSITY', 'GDP_PPP']
sns.pairplot(df[names], hue='GDP_PPP', size=4)

# Create time series
COUNTRY_NAME = 'High income'
ts_hi = df_pivot[COUNTRY_NAME]
ts_hi.plot()
pyplot.show()
COUNTRY_NAME = 'Low income'
ts_lo = df_pivot[COUNTRY_NAME]
ts_lo.plot()
plt.xlabel('Time (in years)')
plt.ylabel('Final energy intensity ratio (Low income country group)')
pyplot.show()

# autocorrelation plot
autocorrelation_plot(ts_lo)
pyplot.show()

# fit model
ts_lo.index = pd.to_datetime(ts_lo.index, format='%Y')
model = ARIMA(ts_lo, order=(5, 1, 0))
model_fit = model.fit(disp=0)
print(model_fit.summary())

# plot residual errors
residuals = DataFrame(model_fit.resid)
residuals.plot()
plt.ylabel('Residuals')
pyplot.show()
residuals.plot(kind='kde')
pyplot.show()
print(residuals.describe())


#  predict
X = ts_lo
size = int(len(X) * 0.8)
train, test = X[0:size-1], X[size-1:len(X)]
history = [x for x in train]
predictions = list()
n = 5
for t in range(len(test)):
    model = ARIMA(history, order=(n, 1, 0))
    model_fit = model.fit(disp=0)
    output = model_fit.forecast()
    yhat = output[0]
    predictions.append(yhat)
    obs = test[t]
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
error = mean_squared_error(test, predictions)
print('Test MSE: %.3f' % error)

# plot
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
l1, = plt.plot(test, label='actual')
l2, = plt.plot(test.index, predictions, color='red', label='predicted')
plt.legend([l1, l2], ['actual', 'predicted'])
plt.xlabel('Time (in years)')
plt.ylabel('Final energy intensity ratio')
plt.title('Predictions for Final Energy Intensity Ratio for \
           Low income country group')
plt.show()
