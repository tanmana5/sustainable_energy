# README #

Execute script to produce plots and analysis results.
Details at : https://medium.com/@tanmana5/sustainable-energy-for-all-data-to-guide-sustainability-initiatives-fe90632b392e

### What is this repository for? ###

* Codes for understanding sustainable development through data analytics. 
* Python 3.6
* Data downloaded from World Bank data portal (http://data.worldbank.org/data-catalog/sustainable-energy-for-all)


### Who do I talk to? ###

* Tanmana Sadhu
